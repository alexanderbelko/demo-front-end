const demo = new Vue({
  el: '#app',
  data: {
    authorized: Boolean(JSON.parse(localStorage.getItem('currentUser'))),
    errorApp: '',
    errorLogin: '',
    loginForm: {
      username: '',
      password: ''
    },
    selectData: [
      { value: "1", text: 'Option #1' },
      { value: "2", text: 'Option #2' },
      { value: "3", text: 'Option #3' },
      { value: "4", text: 'Option #4' },
      { value: "5", text: 'Option #5' },
      { value: "6", text: 'Option #6' }
    ],
    fieldsData: {
      datasets: [],
      ensembles: [],
      notebooks: [],
      initiators: []
    },
    formsDataSets: {
      formA: {
        datasets: [],
        notebooks: [],
        esembles: []
      },
      formB: {
        datasets: [],
        notebooks: [],
        esembles: []
      },
      formC: {
        datasets: [],
        notebooks: [],
        esembles: []
      },
    },
    forms: {
      SingleNotebook: {
        visible: true,
        isLoading: false,
        data: {
          notebook_id: null,
          datasets: [],
          initiator_id: null
        }
      },
      Ensemble: {
        visible: false,
        isLoading: false,
        data: {
          ensemble_id: null,
          datasets: [],
          initiator_id: null
        }
      },
      BuildEnsemble: {
        visible: false,
        isLoading: false,
        data: {
          analytic_notebook_id: null,
          notebook_id: [],
          datasets: [],
          initiator_id: null
        }
      },
      AB: {
        visible: false,
        isLoading: false,
        data: {
          side_1: {
            notebook_id: null,
            datasets: [],
          },
          side_2: {
            notebook_id: null,
            datasets: [],
          },
          analytic_notebook_id: null,
          initiator_id: null
        }
      },
      PerformanceAnalysis: {
        visible: false,
        isLoading: false,
        data: {
          analytic_notebook_id: null,
          notebooks: [],
          initiator_id: null,
          date_from: null,
          date_to: null
        }
      },
    },
    performances: []
  },


  mounted: function() {
    axios.defaults.baseURL = 'http://35.185.79.244:8000';

    if (this.authorized) {
      axios.defaults.headers.common['Authorization'] = 'Token ' + JSON.parse(localStorage.getItem('currentUser')).token;
    }

    $(".js-select").select2();
    $('#PerformanceAnalysis_date_to').datetimepicker();
    $('#PerformanceAnalysis_date_from').datetimepicker();

    $('#PerformanceAnalysis_date_to').on('dp.change', (e) => {
      this.forms.PerformanceAnalysis.data.date_to = moment(e.date._d).format('DD-MM-YYYY');
    });

    $('#PerformanceAnalysis_date_from').on('dp.change', (e) => {
      this.forms.PerformanceAnalysis.data.date_from = moment(e.date._d).format('DD-MM-YYYY');
    });

    axios.get('/notebooks/')
      .then(response => {
        this.fieldsData.notebooks = response.data;
        this.fieldsData.notebooks.forEach(item => {
          if (item.id == 1000 || item.id == 1001 ) {
            this.formsDataSets.formA.notebooks.push(item);
          }
        });
        this.fieldsData.notebooks.forEach(item => {
          if (item.id == 1000) {
            this.formsDataSets.formB.notebooks.push(item);
          }
        });

        console.log(this.formsDataSets.formA.notebooks);
      })
      .catch(e => {
        this.errorApp = e.response;
      });

    axios.get('/users/')
      .then(response => {
        this.fieldsData.initiators.push(response.data[1]);
      })
      .catch(e => {
        this.errorApp = e.response;
      });

    axios.get('/datasets/')
      .then(response => {
        this.fieldsData.datasets = response.data;
        this.fieldsData.datasets.forEach(item => {
          if (item.id === 1000 || item.id === 1008 || item.id === 1001) {
            this.formsDataSets.formA.datasets.push(item);
          }
        });
        this.fieldsData.datasets.forEach(item => {
          if (item.id >= 1002 && item.id <= 1006) {
            this.formsDataSets.formB.datasets.push(item);
          }
        });
        this.fieldsData.datasets.forEach(item => {
          if (item.id === 1000 || item.id === 1008) {
            this.formsDataSets.formC.datasets.push(item);
          }
        });
      })
      .catch(e => {
        this.errorApp = e.response;
      });

    axios.get('/ensembles/')
      .then(response => {
        this.fieldsData.ensembles = response.data;
        this.fieldsData.ensembles.forEach(item => {
          if (item.id == 1001) {
            this.formsDataSets.formC.esembles.push(item);
          }
        });
      })
      .catch(e => {
        this.errorApp = e.response;
      });

    axios.get('/performances')
      .then(response => {
          this.performances = response.data.map(item => {
            item.created_at = moment(item.created_at).format('LLL');
            return item;
          });
      })
      .catch(e => {
        this.errorApp = e.response;
      })
  },


  methods: {
    hideAlert: function() {
      $('.alert').hide();
    },


    submitLogin: function(e) {
      axios.post('/api-token/', this.loginForm)
        .then(response => {
          this.authorized = true;
          localStorage.setItem('currentUser', JSON.stringify({ token: response.data.token, name: this.loginForm.username }));
          axios.defaults.headers.common['Authorization'] = 'Token ' + response.data.token;
        })
        .catch(e => {
          this.errorLogin = e.response;
        });


    },


    switchForm: function(form) {
      for (let one in this.forms) {
        this.forms[one].visible = false;
      }
      this.forms[form].visible = true;
    },


    getDataByPerformanceId: function(id) {
      axios.get('performances/' + id)
        .then(response => {
          console.log(response.data);
        })
        .catch(e => {
          this.errorApp = e;
        })
    },


    SingleNotebookSubmit: function() {
      this.forms.SingleNotebook.data.notebook_id = $('#SingleNotebook_notebook_id').val();
      this.forms.SingleNotebook.data.datasets = $('#SingleNotebook_datasets').val();
      this.forms.SingleNotebook.data.initiator_id = $('#SingleNotebook_initiator_id').val();
      this.forms.SingleNotebook.isLoading = true;
      axios.post('/performing/single-notebook/', this.forms.SingleNotebook.data)
        .then(response => {
          console.log(response);
          this.getDataByPerformanceId(36);
          this.forms.SingleNotebook.isLoading = false;
        })
        .catch(e => {
          this.errorApp = e;
        })
    },


    EnsembleSubmit: function() {
      this.forms.Ensemble.data.ensemble_id = $('#Ensemble_ensemble_id').val();
      this.forms.Ensemble.data.datasets = $('#Ensemble_datasets').val();
      this.forms.Ensemble.data.initiator_id = $('#Ensemble_initiator_id').val();

      axios.post('/performing/ensemble/', this.forms.Ensemble.data)
        .then(response => {
          console.log(response);
        })
        .catch(e => {
          this.errorApp = e;
        })
    },


    BuildEnsembleSubmit: function() {
      this.forms.BuildEnsemble.data.analytic_notebook_id = $('#BuildEnsemble_analytic_notebook_id').val();
      this.forms.BuildEnsemble.data.notebook_id[0] = $('#BuildEnsemble_notebook_id').val();
      this.forms.BuildEnsemble.data.datasets = $('#BuildEnsemble_datasets').val();
      this.forms.BuildEnsemble.data.initiator_id = $('#BuildEnsemble_initiator_id').val();

      axios.post('/performing/build-ensemble/', this.forms.BuildEnsemble.data)
        .then(response => {
          console.log(response);
        })
        .catch(e => {
          this.errorApp = e;
        })
    },


    ABSubmit: function() {
      this.forms.AB.data.side_1.notebook_id = $('#AB_side_1_notebook_id').val();
      this.forms.AB.data.side_1.datasets = $('#AB_side_1_datasets').val();
      this.forms.AB.data.side_2.notebook_id = $('#AB_side_2_notebook_id').val();
      this.forms.AB.data.side_2.datasets = $('#AB_side_2_datasets').val();
      this.forms.AB.data.analytic_notebook_id = $('#AB_analytic_notebook_id').val();
      this.forms.AB.data.initiator_id = $('#AB_initiator_id').val();

      axios.post('/performing/ab-test/', this.forms.AB.data)
        .then(response => {
          console.log(response);
        })
        .catch(e => {
          this.errorApp = e;
        })
    },


    PerformanceAnalysisSubmit: function() {
      console.log($('#PerformanceAnalysis_date_from').val());
      this.forms.PerformanceAnalysis.data.analytic_notebook_id = $('#PerformanceAnalysis_analytic_notebook_id').val();
      this.forms.PerformanceAnalysis.data.notebooks[0] = $('#PerformanceAnalysis_notebooks').val();
      this.forms.PerformanceAnalysis.data.initiator_id = $('#PerformanceAnalysis_initiator_id').val();

      axios.post('/performing/performance-analysis/', this.forms.PerformanceAnalysis.data)
        .then(response => {
          console.log(response);
        })
        .catch(e => {
          this.errorApp = e;
        })
    }
  }
});